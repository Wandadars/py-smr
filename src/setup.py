from setuptools import find_packages, setup

setup(
    name='pysmr',
    packages=find_packages('pysmr'),
    version='0.2.0',
    description='Stochastic Microstructure Reconstruction using Deep Learning',
    author='Christopher Neal',
    license='',
)
