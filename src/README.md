All unit testing files should start with 'test', and they should be placed in the 'tests' directory. It is convention to put the module name that you are testing after the 'test' keyword.
Example: module is my\_functions.py  the unit tests file should be named test\_my\_functions.py

To run all unit test files use: python -m unittest discover


*To analyze the code for quality use: pyline flamelet_table.py >debug.txt  (Just replace the argument with the file to profile)

