import os
import sys
import pytest

import logging

if __name__ == "__main__":
    file_path = os.path.dirname(os.path.abspath(__file__))
    unit_tests_path = os.path.join(os.path.abspath(file_path), "tests/unit_tests")
    integration_tests_path = os.path.join(os.path.abspath(file_path), "tests/integration_tests")
    source_path = os.path.join(os.path.abspath(file_path), "pysmr")
    print('Test Paths: ' + unit_tests_path)
    sys.path.insert(0, unit_tests_path)
    sys.path.insert(0, integration_tests_path)
    sys.path.insert(0, source_path)
    pytest.main(['-x', 'tests'])
