import os
import pytest
import sys
import logging
import unittest

import microstructure_reconstructor
import input_parser as ip

logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('matplotlib').setLevel(logging.WARNING)
logger = logging.getLogger('pysmr: ' + __name__)


class TestMicrostructorImageReconstructor(unittest.TestCase):
    def setUp(self):
        file_path = os.path.dirname(os.path.abspath(__file__)) #For locating files
        input_file_name = os.path.join(os.path.abspath(file_path), 'dummy_input_file_01.txt')
        self.input_parser = ip.InputFileParser(input_file_name)
    
    def test_create_reconstruction(self):
        base_reconstructor = microstructure_reconstructor.MicroscaleImageReconstructor(self.input_parser)
        with pytest.raises(NotImplementedError) as exception:
            reconstruction= base_reconstructor.create_reconstruction()


class TestTensorFlowMicrostructorImageReconstructor(unittest.TestCase):
    def setUp(self):
        file_path = os.path.dirname(os.path.abspath(__file__)) #For locating files
        input_file = os.path.join(os.path.abspath(file_path), 'dummy_input_file_01.txt')

    def test_initializtion(self):
        pass



if __name__ == '__main__':
    unittest.main()
