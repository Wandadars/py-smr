import logging
import os
import pytest
import sys
import unittest

from pysmr import input_parser as ip


logger = logging.getLogger('pysmr_test: ' + __name__)

class TestInputParserClass(unittest.TestCase):
    def setUp(self):
        file_path = os.path.dirname(os.path.abspath(__file__)) #For locating files
        self.input_file_name = os.path.join(os.path.abspath(file_path), 'dummy_input_file_01.txt')
        self.input_parser = ip.InputFileParser(self.input_file_name)
        
    def test_line_splitting(self):
        logger.info(self.input_parser.user_input_data['style_image'])
        assert  self.input_parser.user_input_data['style_image'] == 'dummy_style_image.png'

    def test_nonexistent_file_exception(self):
        with pytest.raises(IOError) as exception:
            ip.InputFileParser('nonexistent_input.txt')

    def test_clean_input_file_data(self):
        data = self.input_parser.clean_input_file_data(self.input_file_name)
        assert '#' not in data
        

if __name__ == '__main__':
    unittest.main()
