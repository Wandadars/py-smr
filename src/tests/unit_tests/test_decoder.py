import os
import sys
import logging
import pytest
import unittest

import numpy as np

from pysmr import decoder
from pysmr import input_parser as ip
from pysmr import image_processing_utils as ipu

logging.getLogger('tensorflow').setLevel(logging.WARNING)
logger = logging.getLogger('pysmr: ' + __name__)


class TestTwoPhaseMicrostructureDecoderClass(unittest.TestCase):
    def setUp(self):
        file_path = os.path.dirname(os.path.abspath(__file__)) #For locating files
        input_file_name = os.path.join(os.path.abspath(file_path), 'dummy_input_file_for_decoder_testing.txt')

        self.input_parser = ip.InputFileParser(input_file_name)
    
    def test_map_pixels_to_discrete_values(self):
        """
        Create a 10x10x3 numpy array to represent an image, and 
        make half of it off-white and the other half off-black. Then
        use the decoder to map the pixels to full black and white. The
        ratio should be 50% black and 50% white.
        """
        test_image = np.zeros((10, 10, 3))
        test_image[0:5, :, :] = 240
        test_image[5:, :, :] = 20
        logger.debug('Test Image:')
        logger.debug(test_image)
        unique_pixel_values = ipu.unique_pixel_values(test_image)

        test_phase_colors = {'phase_1': [0, 0, 0], 'phase_2': [255, 255, 255]}

        two_phase_decoder = decoder.TwoPhaseMicrostructureDecoder(self.input_parser)
        two_phase_decoder.set_phase_colors(test_phase_colors)
        decoded_image = two_phase_decoder.decode_image(test_image)

        unique_pixel_values = ipu.unique_pixel_values(decoded_image)
        assert unique_pixel_values[1][0] == unique_pixel_values[1][1]

    def test_set_phase_colors(self):
        """
        Ensure that the dictionary that is passed to the set_phase_colors method
        properly sets the attribute in the object.
        """
        test_phase_colors = {'phase_a': [0, 0, 1], 'phase_b': [0, 1, 0]}
        two_phase_decoder = decoder.TwoPhaseMicrostructureDecoder(self.input_parser)
        two_phase_decoder.set_phase_colors(test_phase_colors)
        assert two_phase_decoder.phase_colors['phase_a'] == [0, 0, 1]



if __name__ == '__main__':
    unittest.main()
