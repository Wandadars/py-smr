import os
import sys
import logging
import pytest
import unittest

import numpy as np
import tensorflow as tf

from pysmr import encoder
from pysmr import losses
from pysmr import input_parser as ip

logging.getLogger('tensorflow').setLevel(logging.WARNING)
logging.getLogger('matplotlib').setLevel(logging.WARNING)
logger = logging.getLogger('pysmr: ' + __name__)

tf.enable_eager_execution()

class TestGenericLossEvaluator(unittest.TestCase):
    def test_compute_loss_weights(self):
        loss_evaluator = losses.GenericLossEvaluator()
        with pytest.raises(NotImplementedError) as exception:
            loss_evaluator.compute_loss_weights()

    def test_compute_total_loss(self):
        loss_evaluator = losses.GenericLossEvaluator()
        with pytest.raises(NotImplementedError) as exception:
            loss_evaluator.compute_total_loss(style_transfer_state=None)


class TestLossEvaluator(unittest.TestCase):
    def setUp(self):
        file_path = os.path.dirname(os.path.abspath(__file__)) #For locating files
        input_file_name = os.path.join(os.path.abspath(file_path), 'dummy_input_file_for_encoder_testing.txt')
        self.input_parser = ip.InputFileParser(input_file_name)
    
    def test_compute_loss_weights(self):
        loss_evaluator = losses.LossEvaluator(self.input_parser)
        with pytest.raises(NotImplementedError) as exception:
            encoded_image = loss_evaluator.compute_loss_weights()

    def test_compute_loss(self):
        loss_evaluator = losses.LossEvaluator(self.input_parser)
        #with pytest.raises(NotImplementedError) as exception:
        #    loss = loss_evaluator.compute_loss()


class TestGenericLossCalculator(unittest.TestCase):
    def test_compute_loss(self):
        loss_calculator = losses.GenericLossCalculator()
        with pytest.raises(NotImplementedError) as exception:
            loss_calculator.compute_loss()


class TestStyleLossCalculator(unittest.TestCase):
    def setUp(self):
        file_path = os.path.dirname(os.path.abspath(__file__)) #For locating files
        input_file_name = os.path.join(os.path.abspath(file_path), 'dummy_input_file_for_encoder_testing.txt')
        self.input_parser = ip.InputFileParser(input_file_name)
        self.style_loss_calculator = losses.StyleLossCalculator(None)

    def test_gram_matrix(self):
        tensor = tf.convert_to_tensor(2 * np.ones((2,2,2)), dtype=tf.float32)
        logger.debug(tensor)
        gram = self.style_loss_calculator.gram_matrix(tensor)

        logger.debug(gram)
        assert np.array_equal(gram, np.ones((4,4)))


class TestInterLayerStyleLossCalculator(unittest.TestCase):
    def setUp(self):
        file_path = os.path.dirname(os.path.abspath(__file__)) #For locating files
        input_file_name = os.path.join(os.path.abspath(file_path), 'dummy_input_file_for_encoder_testing.txt')
        self.input_parser = ip.InputFileParser(input_file_name)
        self.style_loss_calculator = losses.InterLayerStyleLossCalculator(None)

    def test_vectorize_feature_map(self):
        height = 3
        width = 3
        channels = 2
        conv_layer_representation = np.ones((height, width, channels))
        vectorized_feature = self.style_loss_calculator.vectorize_feature_map(conv_layer_representation)
        assert vectorized_feature.shape == (height * width, channels)
        
    def test_upsample_feature_map(self):
        height = 3
        width = 3
        channels = 2
        conv_layer_representation = np.ones((height, width, channels))
        feature = tf.convert_to_tensor(conv_layer_representation, np.float32)
        
        new_height = 4
        new_width = 4
        new_channels = 7
        new_size = (new_height, new_width, new_channels) 
        
        upsampled_feature = self.style_loss_calculator.upsample_feature_map(feature, new_size)
        assert upsampled_feature.shape == (new_height * new_width, new_channels)

    def test_gram_matrix(self):
        pass


if __name__ == '__main__':
    unittest.main()
