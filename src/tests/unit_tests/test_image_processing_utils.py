import os
import sys
import logging
import PIL
import pytest
import unittest

import numpy as np

from pysmr import image_processing_utils as ipu

logger = logging.getLogger('pysmr: ' + __name__)


class TestImageProcessingUtilsModule(unittest.TestCase):
    def setUp(self):
        pass
    
    def test_convert_palettized_image_to_rgb(self):
        palettedata = [0, 0, 0, 102, 102, 102, 176, 176, 176, 255, 255, 255]
        image = PIL.Image.new('P', (10, 10))
        image.putpalette(palettedata *32)
        
        logger.debug('Palettized Image test')
        logger.debug(image.getbands())
        
        rgb_image = ipu.convert_palettized_image_to_rgb(image)
        assert rgb_image.getbands() == ('R', 'G', 'B')

    def test_remove_alpha_channel(self):
        """
        Create a 4D array to represent an RGBA image and test
        if the Alpha channel is properly removed by the method.
        """
        rgba_array = np.zeros((10,10,3,1))
        rgba_image = PIL.Image.fromarray(rgba_array, mode='RGBA')
        rgb_image = ipu.remove_alpha_channel(rgba_image)
        assert rgb_image.getbands() == ('R', 'G', 'B')
        
    def test_unique_pixel_values(self):
        """
        Tests whether a dummy image of 0's and 255's is properly identified
        as having two unique pixel values in an array(representing an image) of
        0 and 255.
        """
        test_image = np.zeros((10, 10, 3))
        test_image[0:5, :, :] = 255
        test_image[5:, :, :] = 0
        unique_pixel_values = ipu.unique_pixel_values(test_image)
        assert unique_pixel_values[1][0] == unique_pixel_values[1][1]

    def test_compute_phase_color_clusters(self):
        test_image = np.zeros((10, 10, 3))
        test_image[0:5, :, :] = 255
        test_image[5:, :, :] = 0
        number_of_clusters = 2
        phase_colors = ipu.compute_phase_color_clusters(number_of_clusters, test_image)
        print(phase_colors)
        print(phase_colors.values())
        phase_colors = [item.tolist() for item in phase_colors.values()]
        print(phase_colors)
        assert [255, 255, 255] in phase_colors and [0, 0, 0] in phase_colors

    def test_map_pixels_to_discrete_values(self):
        """
        Create a 10x10x3 numpy array to represent an image, and 
        make half of it off-white and the other half off-black. Then
        use the decoder to map the pixels to full black and white. The
        ratio should be 50% black and 50% white.
        """
        test_image = np.zeros((10, 10, 3))
        test_image[0:5, :, :] = 240
        test_image[5:, :, :] = 20
        logger.debug('Test Image:')
        logger.debug(test_image)
        unique_pixel_values = ipu.unique_pixel_values(test_image)

        test_phase_colors = {'phase_1': [0, 0, 0], 'phase_2': [255, 255, 255]}

        mapped_image = ipu.map_pixels_to_discrete_values(test_image, test_phase_colors)
        
        unique_pixel_values = ipu.unique_pixel_values(mapped_image)
        assert unique_pixel_values[1][0] == unique_pixel_values[1][1]



if __name__ == '__main__':
    unittest.main()
