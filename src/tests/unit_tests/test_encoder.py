import os
import sys
import logging
import pytest
import unittest

import numpy as np

from pysmr import encoder
from pysmr import input_parser as ip

logging.getLogger('tensorflow').setLevel(logging.WARNING)
logging.getLogger('matplotlib').setLevel(logging.WARNING)
logger = logging.getLogger('pysmr: ' + __name__)


class TestGenericEncoderClass(unittest.TestCase):
    def test_encode_image(self):
        base_encoder = encoder.GenericEncoder()
        with pytest.raises(NotImplementedError) as exception:
            encoded_image = base_encoder.encode_image()

    def test_load_image(self):
        base_encoder = encoder.GenericEncoder()
        with pytest.raises(NotImplementedError) as exception:
            encoded_image = base_encoder.load_image('')
    
    def test_output_image(self):
        base_encoder = encoder.GenericEncoder()
        with pytest.raises(NotImplementedError) as exception:
            encoded_image = base_encoder.output_image()


class TestMicrostructureEncoderClass(unittest.TestCase):
    def setUp(self):
        file_path = os.path.dirname(os.path.abspath(__file__)) #For locating files
        input_file_name = os.path.join(os.path.abspath(file_path), 'dummy_input_file_for_encoder_testing.txt')
        self.input_parser = ip.InputFileParser(input_file_name)
    
    def test_load_image(self):
        pass


class TestTwoPhaseMicrostructureEncoderClass(unittest.TestCase):
    def setUp(self):
        file_path = os.path.dirname(os.path.abspath(__file__)) #For locating files
        input_file_name = os.path.join(os.path.abspath(file_path), 'dummy_input_file_for_encoder_testing.txt')

        self.input_parser = ip.InputFileParser(input_file_name)
    
    def test_encode_image(self):
        two_phase_encoder = encoder.TwoPhaseMicrostructureEncoder(self.input_parser)
        encoded_image = two_phase_encoder.encode_image()

    def test_add_batch_dimension_to_image(self):
        two_phase_encoder = encoder.TwoPhaseMicrostructureEncoder(self.input_parser)
        rgb_array = np.zeros((10,10,3))
        batch_rgb_array = two_phase_encoder.add_batch_dimension_to_image(rgb_array)
        assert len(batch_rgb_array.shape) == 4


if __name__ == '__main__':
    unittest.main()
