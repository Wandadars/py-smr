style_image  ellipse_30vf_16ab_theta1.png 
style_weight  10
total_variation_weight 1e-20

num_iterations	4000
learning_rate 40

model VGG19Model1
losses hmx_loss
encoder two_phase
decoder two_phase
optimizer adam

output_prefix  test_output
output_height 500
output_width 500

