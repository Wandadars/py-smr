style_image  ClassV_sizestudy_Sample2_ds_segmented.png 
style_weight  10
total_variation_weight 1e-20

num_iterations	12000

model_type	VGG19Model1
losses hmx_loss
encoder two_phase
decoder two_phase
optimizer adam

learning_rate 2

output_prefix  test_output
output_height 300
output_width 300

