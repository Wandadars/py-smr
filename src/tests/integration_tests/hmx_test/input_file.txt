style_image  hmx_test_image.png
style_weight  10
total_variation_weight  1e-10

#num_iterations	10000
num_iterations	2000

model VGG19Model1
losses hmx_loss

encoder two_phase
decoder two_phase
optimizer adam

learning_rate 1
regularization_freq 500

output_prefix  test_output
output_height 200
output_width 200

