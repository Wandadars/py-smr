import logging

import numpy as np
import tensorflow as tf
from tensorflow.python.keras import backend as K

import global_settings
import image_processing_utils as ipu


logger = logging.getLogger('pysmr:' + __name__)

class GenericLossEvaluator:
    def compute_loss_weights(self):
        raise NotImplementedError

    def compute_total_loss(self, style_transfer_state):
        raise NotImplementedError


class LossEvaluator(GenericLossEvaluator):
    """
    The responsibility of this class is to collect losses from a set of loss
    objects.
    """
    def __init__(self, input_file_parser):
        self.user_input_data = input_file_parser.user_input_data
        self.loss_objects = None
        self.loss_weights = {}

    def compute_total_loss(self, style_transfer_state):
        total_loss = 0
        loss = {}
        for loss_object in self.loss_objects:
            loss[str(loss_object)] = self.loss_weights[str(loss_object)] * loss_object.compute_loss(**style_transfer_state)
        total_loss = sum([loss[key] for key in loss])
        loss['total_loss'] = total_loss
        return loss
    
    def compute_loss_weights(self):
        raise NotImplementedError


class HMXLossEvaluator(LossEvaluator):
    def __init__(self, input_file_parser, model, image_decoder):
        super().__init__(input_file_parser)
        self.loss_objects = [StyleLossCalculator(model),
                             #InterLayerStyleLossCalculator(model),
                             TotalVariationLossCalculator()]
        self.loss_weights = self.compute_loss_weights() 

    def compute_loss_weights(self):
        loss_weights = {}
        try:
            style_weight = float(self.user_input_data['style_weight'])
        except KeyError:
            raise('style_weight not specified in input file')
        logger.debug('Style Weight: ' + str(style_weight))
        loss_weights['style_loss'] = style_weight
        
        #Use the same weight
        #logger.debug('Inter Layer Style Weight: ' + str(style_weight))
        #loss_weights['inter_layer_style_loss'] = style_weight
        
        try:
            total_variation_weight = float(self.user_input_data['total_variation_weight'])
        except KeyError:
            raise('total_variation_weight not specified in input file')
        logger.debug('Total Variation Weight: ' + str(total_variation_weight))
        loss_weights['total_variation_loss'] = total_variation_weight
        
        return loss_weights

class InterLayerLossEvaluator(HMXLossEvaluator):
    def __init__(self, input_file_parser, model, image_decoder):
        super().__init__(input_file_parser, model, image_decoder)
        self.loss_objects = [StyleLossCalculator(model),
                             InterLayerStyleLossCalculator(model),
                             TotalVariationLossCalculator()]
        self.loss_weights = self.compute_loss_weights() 

    def compute_loss_weights(self):
        loss_weights = {}
        try:
            style_weight = float(self.user_input_data['style_weight'])
        except KeyError:
            raise('style_weight not specified in input file')
        logger.debug('Style Weight: ' + str(style_weight))
        loss_weights['style_loss'] = style_weight
        
        loss_weights['inter_layer_style_loss'] = style_weight
        
        #Use the same weight
        #logger.debug('Inter Layer Style Weight: ' + str(style_weight))
        #loss_weights['inter_layer_style_loss'] = style_weight
        
        try:
            total_variation_weight = float(self.user_input_data['total_variation_weight'])
        except KeyError:
            raise('total_variation_weight not specified in input file')
        logger.debug('Total Variation Weight: ' + str(total_variation_weight))
        loss_weights['total_variation_loss'] = total_variation_weight
        
        return loss_weights


        
class HMXLossVolumeFractionEvaluator(LossEvaluator):
    def __init__(self, input_file_parser, model, image_decoder):
        super().__init__(input_file_parser)
        self.loss_objects = [StyleLossCalculator(model),
                             TotalVariationLossCalculator(),
                             self.create_volume_fraction_loss(image_decoder, model)]
        self.loss_weights = self.compute_loss_weights() 

    def create_volume_fraction_loss(self, image_decoder, model):
        #Get primary phase label from input data
        try:
            primary_phase_label = int(self.user_input_data['primary_phase_label'])
        except KeyError:
            raise('primary_phase_label not specified in input file')
        
        #get target volume fractoin from input data
        try:
            volume_fraction_target = float(self.user_input_data['volume_fraction_target'])
        except KeyError:
            raise('volume_fraction_target not specified in input file')

        #instantialte object
        return VolumeFractionLossCalculator(primary_phase_label, volume_fraction_target, image_decoder, model)

    def compute_loss_weights(self):
        loss_weights = {}
        try:
            style_weight = float(self.user_input_data['style_weight'])
        except KeyError:
            raise('style_weight not specified in input file')
        logger.debug('Style Weight: ' + str(style_weight))
        loss_weights['style_loss'] = style_weight
        
        try:
            total_variation_weight = float(self.user_input_data['total_variation_weight'])
        except KeyError:
            raise('total_variation_weight not specified in input file')
        logger.debug('Total Variation Weight: ' + str(total_variation_weight))
        loss_weights['total_variation_loss'] = total_variation_weight
        
        try:
            volume_fraction_weight = float(self.user_input_data['volume_fraction_weight'])
        except KeyError:
            raise('volume_fraction_weight not specified in input file')
        logger.debug('Volume Fraction Weight: ' + str(volume_fraction_weight))
        loss_weights['volume_fraction_loss'] = volume_fraction_weight
            
        return loss_weights

        



class GenericLossCalculator:
    def compute_loss(self, **kwargs):
        raise NotImplementedError


class StyleLossCalculator(GenericLossCalculator):
    def __init__(self, model):
        self.model = model
        self.style_image_features = None
        self.gram_style_features = None

    def __str__(self):
        return 'style_loss'

    def compute_loss(self, **kwargs):
        """
        This function will compute the loss total loss.

        Args:
            image: Initial reconstruction image. This image is being updating with 
                   the optimization process. Apply the gradients wrt the loss we are 
                   calculating to this image.
            style_image_features: Precomputed style image feature maps for layers of interest.
          
        Returns:
            style loss:
        """
        image = kwargs['image']
        style_image_features = kwargs['style_image_features']
        model = self.model.get_model() #The actual model from the model container

        # Feed initial image through the model. This gives the style representation 
        # at the desired layers. 
        generated_image_features = model(image)
        if style_image_features != self.style_image_features:
            logger.info('Computing Style Features for Style Loss')
            self.style_image_features = style_image_features
            self.gram_style_features = [self.gram_matrix(feature) for feature in style_image_features]
        
        # Accumulate style losses from all layers
        style_layer_weights = self.model.style_layer_weights()
        #Note, the [0] is just to access the batch dimension data for the first batch element
        style_loss = 0
        iterables = zip(self.gram_style_features, generated_image_features, style_layer_weights)
        for gram_style_feature, generated_image_feature, style_layer_weight in iterables:
            style_loss += style_layer_weight * self.get_style_loss(generated_image_feature[0],
                                                                   gram_style_feature)
        return style_loss 

    def get_style_loss(self, input_style, gram_target):
        """Expects image of dimension h, w, c"""
        # height, width, num filters of each layer
        # Scale the loss at a given layer by the size of the feature map and the number of filters
        input_style = tf.cast(input_style, tf.float32)
        gram_target = tf.cast(gram_target, tf.float32)

        height, width, channels = input_style.get_shape().as_list()
        gram_style = self.gram_matrix(input_style)
        return tf.reduce_mean(tf.square(gram_style - gram_target)) / (4. * (channels ** 2) * (width * height) ** 2)
    
    def gram_matrix(self, input_tensor):
        """
        Computes the Gram matrix for a provided 3D tensor of shape (H,W,C) by
        reducing the dimension of the input tensor by one to create a (H*W,C) shaped
        2D tensor and then computing the Gramian Matrix using that 2D tensor.
        """
        input_tensor = tf.cast(input_tensor, tf.float32)

        activation_shift = -1 #Used for centering activations around 0 to reduce gram sparsity
        channels = int(input_tensor.shape[-1]) # store the number of image channels
        a = tf.reshape(input_tensor, [-1, channels]) #[H*W, C]
        n = tf.shape(a)[0]
        gram = tf.matmul((a + activation_shift), (a + activation_shift), transpose_a=True)
        return gram / tf.cast(n, tf.float32)


class InterLayerStyleLossCalculator(GenericLossCalculator):
    def __init__(self, model):
        self.model = model
        self.style_image_features = None
        self.inter_layer_gram_style_features = None

    def __str__(self):
        return 'inter_layer_style_loss'

    def compute_loss(self, **kwargs):
        """
        This function will compute the loss total loss.

        Args:
            image: Initial reconstruction image. This image is being updating with 
                   the optimization process. GrWe apply the gradients wrt the loss we are 
                   calculating to this image.
            style_image_features: Precomputed style image feature maps for layers of interest.
          
        Returns:
            style loss:
        """
        image = kwargs['image']
        style_image_features = kwargs['style_image_features']
        model = self.model.get_model()

        # Feed initial image through the model. This gives the style representation 
        # at the desired layers. 
        generated_image_features = model(image)
        if style_image_features != self.style_image_features:
            #Compute the chain gram correlations for the style image
            self.style_image_features = style_image_features
            self.inter_layer_gram_style_features = []
            for i in range(1, len(self.style_image_features)):
                upsampled_feature = self.upsample_feature_map(self.style_image_features[i],
                                                              self.style_image_features[i-1].shape)
                gram = self.inter_layer_gram_matrix(upsampled_feature,
                                                    self.vectorize_feature_map(self.style_image_features[i-1]))
                self.inter_layer_gram_style_features.append(gram)

        style_layer_weights = self.model.style_layer_weights()
        correlation_style_loss = 0
        for i, generated_image_feature in enumerate(generated_image_features):
            if i > 0:
                avg_layer_weight = 0.5 * (style_layer_weights[i] + style_layer_weights[i-1])
                chained_loss = self.get_chained_style_loss(generated_image_features[i][0],
                                                           generated_image_features[i-1][0],
                                                           self.inter_layer_gram_style_features[i-1])
                correlation_style_loss += avg_layer_weight * chained_loss 
        return correlation_style_loss
    
    def vectorize_feature_map(self, feature_map):
        channels = int(feature_map.shape[-1]) # store the number of channels
        return tf.reshape(feature_map, [-1, channels])
    
    def upsample_feature_map(self, small_feature, new_shape):
        """
        Upsamples the feature vector to a larger size

        Arguments:
            small_feature: a 3D array [H, W, C] that represents a convolutional layer
            new_shape: A tuple containing the size of the desired feature (3D) [H_new, Width_new, Channels_new]
            
        Returns:
            upsampled_features: A 2D array [H_new*Width_new, Channels_new] representing the upsampled features
                                in a 2D form.
        """
        height, width, channels = new_shape
        new_size = tf.Variable([height*width, channels], dtype=tf.int32)
        #Resize to match the size of the second feature map
        vectorized_feature = self.vectorize_feature_map(small_feature)
        padded_feature_vector = tf.expand_dims(vectorized_feature, 2)
        upsampled_features = tf.image.resize_images(padded_feature_vector, new_size, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
        upsampled_features = tf.squeeze(upsampled_features)
        return upsampled_features
    
    def get_chained_style_loss(self, style_feature_1, style_feature_2, gram_target):
        """
        Assumed that style_feature_1 is smaller in size than style_feature_2 as style_feature_1
        would be coming from a deeper layer in the network.
        """
        # Scale the loss at a given layer by the size of the feature map and the number of filters
        height_2, width_2, channels_2 = style_feature_2.get_shape().as_list()
        upsampled_features_1 = self.upsample_feature_map(style_feature_1, style_feature_2.shape) 
        gram_style = self.inter_layer_gram_matrix(upsampled_features_1, style_feature_2)
        return tf.reduce_mean(tf.square(gram_style - gram_target)) / (4. * (channels_2 ** 2) * (width_2 * height_2) ** 2)
    
    
    def inter_layer_gram_matrix(self, vector_1, tensor_2):
        """
        The two tensors need to be the same shape.
        """
        activation_shift = -1 #Used for centering activations around 0 to reduce gram sparsity
        channels = int(tensor_2.shape[-1]) # store the number of channels
        vector_2 = tf.reshape(tensor_2, [-1, channels])
        n = tf.shape(vector_2)[0]
        gram = tf.matmul((vector_1 + activation_shift), (vector_2 + activation_shift), transpose_a=True)
        return gram / tf.cast(n, tf.float32)


class TotalVariationLossCalculator(GenericLossCalculator):
    """
    A high-pass filter to penalize high frequency noise
    in a generated image.
    """
    def __str__(self):
        return 'total_variation_loss'

    def compute_loss(self, **kwargs):
        """
        This function will compute the loss total loss.

        Args:
            image: Initial reconstruction image. 
        Returns:
            total variation loss:
        """
        image = tf.cast(kwargs['image'], tf.float32)
        return tf.reduce_sum(tf.image.total_variation(image))


class HistogramLossCalculator(GenericLossCalculator):
    def __init__(self, model):
        self.model = model
        self.style_image_features = None
        self.gram_style_features = None

    def __str__(self):
        return 'style_loss'

    def compute_loss(self, **kwargs):
        """
        This function will compute the loss total loss.

        Args:
            image: Initial reconstruction image. This image is being updating with 
                   the optimization process. GrWe apply the gradients wrt the loss we are 
                   calculating to this image.
            style_image_features: Precomputed style image feature maps for layers of interest.
          
        Returns:
            style loss:
        """
        image = kwargs['image']
        style_image_features = kwargs['style_image_features']
        model = self.model.get_model() #The actual model from the model container

        # Feed initial image through the model. This gives the style representation 
        # at the desired layers. 
        generated_image_features = model(image)
        if style_image_features != self.style_image_features:
            logger.info('Computing Style Features for Style Loss')
            self.style_image_features = style_image_features
            self.gram_style_features = [self.gram_matrix(feature) for feature in style_image_features]
        
        # Accumulate style losses from all layers
        style_layer_weights = self.model.style_layer_weights()
        #Note, the [0] is just to access the batch dimension data for the first batch element
        style_loss = 0
        iterables = zip(self.gram_style_features, generated_image_features, style_layer_weights)
        for gram_style_feature, generated_image_feature, style_layer_weight in iterables:
            style_loss += style_layer_weight * self.get_style_loss(generated_image_feature[0],
                                                                   gram_style_feature)
        return style_loss 

    def get_style_loss(self, input_style, gram_target):
        """Expects image of dimension h, w, c"""
        # height, width, num filters of each layer
        # Scale the loss at a given layer by the size of the feature map and the number of filters
        height, width, channels = input_style.get_shape().as_list()
        gram_style = self.gram_matrix(input_style)
        return tf.reduce_mean(tf.square(gram_style - gram_target)) / (4. * (channels ** 2) * (width * height) ** 2)
    
    def gram_matrix(self, input_tensor):
        """
        Computes the Gram matrix for a provided 3D tensor of shape (H,W,C) by
        reducing the dimension of the input tensor by one to create a (H*W,C) shaped
        2D tensor and then computing the Gramian Matrix using that 2D tensor.
        """
        activation_shift = -1 #Used for centering activations around 0 to reduce gram sparsity
        channels = int(input_tensor.shape[-1]) # store the number of image channels
        a = tf.reshape(input_tensor, [-1, channels])
        n = tf.shape(a)[0]
        gram = tf.matmul((a + activation_shift), (a + activation_shift), transpose_a=True)
        return gram / tf.cast(n, tf.float32)


class VolumeFractionLossCalculator(GenericLossCalculator):
    def __init__(self, primary_phase_label, volume_fraction_target, image_decoder, model):
        self.volume_fraction_target = volume_fraction_target
        self.primary_phase_label = 'phase_' + str(primary_phase_label)
        self.image_decoder = image_decoder
        self.model = model

    def __str__(self):
        return 'volume_fraction_loss'
    
    def compute_loss(self, **kwargs):
        loss = 0
        if kwargs['image'] is not None:
            image = self.model.deprocess_image(kwargs['image'].numpy())
            binarized_image = self.image_decoder.binarize_image(image)
            volume_fraction = self.compute_volume_fraction(binarized_image)
            loss = np.abs(self.volume_fraction_target - volume_fraction)
        logger.debug('Volume Fraction Loss')
        logger.debug(loss)
        return loss
        
    def compute_volume_fraction(self, image):
        unique_pixel_vals = ipu.unique_pixel_values(image)
        phase_colors = self.image_decoder.phase_colors
        primary_phase_loc = -1
        for i, pixels in enumerate(unique_pixel_vals[0]):
            if pixels.all() == phase_colors[self.primary_phase_label].all():
                primary_phase_loc = i

        #volume fraction is the ratio of the primary phase to the secondary phase
        num_non_primary_phase_pixels = 0
        for i, phase in enumerate(unique_pixel_vals[0]):
            if i != primary_phase_loc:
                num_non_primary_phase_pixels += unique_pixel_vals[1][i]
        volume_fraction = unique_pixel_vals[1][primary_phase_loc] / num_non_primary_phase_pixels
        logger.debug('Volume Fraction:')
        logger.debug(volume_fraction)
        return volume_fraction



