import argparse
import logging
import os
from PIL import Image
import random
import sys

logger = logging.getLogger(__name__)


def setup_logger():
    """
    Creates logging handler and sets up logging output format.
    """
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)

    logging.basicConfig(format='%(asctime)s %(name)-12s %(levelname)-8s: %(message)s',
                        datefmt='%m/%d/%Y %H:%M %p', filename='pysmr-utility.log', filemode='w',
                        level=logging.DEBUG)

    #For console output from logging
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)
    logging.info('Logger setup complete')


class ImageSubsampler():
    def __init__(self, sampling_image_path):
        self.base_image = None
        self.sampling_image_path = sampling_image_path
        self.base_image_height = None
        self.base_image_width = None
        self.generated_samples_output_path = None

    def generate_samples(self, num_samples, window):
        """
        Input:
            window: Dictionary with height and width of sample window to use. Keys are "height" and "width".
        """
        self.base_image = self.open_image(self.sampling_image_path)
        self.create_output_directory()
        sampled_images = []
        for sample_num in range(num_samples):
            sample_image_data = self.random_crop(self.base_image, window['height'], window['width'])
            sample_image_data['name'] = 'sample_' + str(sample_num + 1)
            sampled_images.append(sample_image_data)
        
        self.output_samples(sampled_images)

    def open_image(self, image_to_open_path):
        try:
            image = Image.open(image_to_open_path)
        except:
            raise('Image failed to open')
        return image

    def random_crop(self, full_image, crop_height, crop_width):
        width, height = full_image.size
        valid_sample_location = False
        while valid_sample_location == False:
            height_location = random.randint(0, height - 1)
            width_location = random.randint(0, width - 1)
            #Make sure the crop fits within the original image
            if height_location + crop_height <= height -1 and width_location + crop_width <= width -1:
                valid_sample_location = True

        cropped = full_image.crop((width_location, height_location, width_location + crop_width, height_location + crop_height))
        return {'image': cropped, 'width_location': width_location,
                'height_location': height_location, 'height': crop_height,
                'width': crop_width}
 
    def output_samples(self, generated_samples):
        for generated_sample in generated_samples:
            full_output_path = os.path.join(self.generated_samples_output_path, generated_sample['name'] + '.png')
            generated_sample['image'].save(full_output_path)

        with open('sample_data.txt', 'w+') as f:
            for generated_sample in generated_samples:
                output_string = '{0}, x_loc: {1}, y_loc: {2}, width: {3}, height: {4}\n'.format(generated_sample['name'], generated_sample['width_location'],
                                                                                                generated_sample['height_location'], generated_sample['width'],
                                                                                                generated_sample['height'])
                f.write(output_string)

    def create_output_directory(self):
        current_dir = os.getcwd()
        output_dir = current_dir + '/output'
        try:
            os.makedirs(output_dir)
        except FileExistsError:
            # directory already exists
            pass
        logger.info('Output directory is: ' + output_dir)
        self.generated_samples_output_path = output_dir


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate random croppings from image')
    parser.add_argument('--image', type=str, help='Image to take croppings from', required=True)
    parser.add_argument('--num_crops', type=int, help='Number of croppings to make from the input image', required=True)
    parser.add_argument('--crop_window', nargs=2, metavar=('crop_width', 'crop_height'), type=int, help='Size of cropping window', required=True)
    args = parser.parse_args()

    setup_logger()
    image_path = args.image
    crop_window = {'width': args.crop_window[1], 'height':args.crop_window[1]}
    image_sampler = ImageSubsampler(image_path)
    image_sampler.generate_samples(args.num_crops, crop_window)

