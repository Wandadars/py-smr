import logging
import PIL

import matplotlib.pyplot as plt
import numpy as np
import sklearn.cluster as sklcluster

from tensorflow.python.keras.preprocessing import image as kp_image
from tensorflow.python.keras import backend as K

import image_processing_utils as ipu

logger = logging.getLogger(__name__)


class GenericEncoder:
    def encode_image(self):
        raise NotImplementedError

    def load_image(self, image_path):
        raise NotImplementedError

    def output_image(self):
        raise NotImplementedError


class MicrostructureEncoder(GenericEncoder):
    """
    The responsibility of this class is to load an image file, process it into
    a RGB output format where the choice of the RGB pixel values depends on the
    input image type and the type of encoding to perform to map scalar pixel
    values to the RGB space.

    The main data format is the Pillow module Image class.
    """
    def __init__(self, input_file_parser):
        self.user_input_data = input_file_parser.user_input_data
        self.encoded_image = None

    def load_image(self, image_path):
        try:
            image = PIL.Image.open(image_path)
        except Exception as e:
            logger.error('Error Reading Image', exc_info=True)
            raise Exception('Error Reading Image: ' + str(e))

        #Convert to RGB if image is Palletized, mode='P'
        image = ipu.convert_palettized_image_to_rgb(image) 
        #Convert 1-bit pixel image to RGB
        image = ipu.convert_1_bit_pixel_image_to_rgb(image)
        #Convert 8-bit pixel image to RGB
        image = ipu.convert_8_bit_pixel_image_to_rgb(image)
        #Remove Alpha channel if it exists
        image = ipu.remove_alpha_channel(image)
 
        logger.debug('Loaded image: ' + image_path)
        logger.debug('Image size: ' + str(image.size))
        logger.debug('Image color band data: ' + str(image.getbands()))
        logger.debug('Image colors (count, (color data): ' + str(image.getcolors()))
        logger.debug('Processed image data type: ' + str(type(image)))
        return image

    def add_batch_dimension_to_image(self, image):
        """
        Prepends a dimension onto the input numpy array.

        Args:   
            image: 3D Numpy array of the form [H,W,C]

        Returns:
            Numpy array of dimension [Batch,H,W,C]

        """
        image = np.expand_dims(image, axis=0) # Broadcast image array to give it a batch dimension
        logger.debug('Processed Image shape (with batch dimension prepended): ' + str(image.shape))
        return image
    
    def pick_image_resizer(self):
        if 'max_dim' in self.user_input_data:
            image_resizer = self.rescale_image_to_fixed_size 
        else:
            image_resizer = self.no_rescaling_image
        return image_resizer

    def no_rescaling_image(self, image):
        return image

    def rescale_image_to_fixed_size(self, image):
        """
        Rescales image to have a maximum dimension and respect aspect ratio.

        Args:
            image: Pillow Image object representing an image with range [0, 255]

        Returns:
            Resized image [Height, Width, Channels] in Pillow Image format
        """
        try:
            max_dim = int(self.user_input_data['max_dim'])
        except Exception as e:
            logger.error('Unable to cast value for max_dim to float')
            raise(e)
        logger.info('Resizing input image to have max dimension of: ' + str(max_dim))
        logger.info('Input image prior to resizing: ' + str(image.size))
        longest_dimension = max(image.size)
        scale = max_dim / longest_dimension
        new_width = round(image.size[0]*scale)
        new_height = round(image.size[1]*scale)
        resized_image = image.resize((new_width, new_height), resample=PIL.Image.ANTIALIAS)
        logger.info('Resized input image size: ' + str(resized_image.size))
        self.output_image(np.asarray(resized_image))
        return resized_image
    
    def output_image(self, image):
        fig, ax = plt.subplots(frameon=False)
        ax.set_axis_off()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        ax.imshow(image.astype('uint8'))
        output_name = 'resized_encoded_image.png'
        plt.imsave(output_name, image.astype('uint8'))
        plt.close(fig)


class TwoPhaseMicrostructureEncoder(MicrostructureEncoder):
    """
    This class handles images that have two phases clearly marked via
    pixel intensity value for a 2D image.
    """
    def __init__(self, input_file_parser):
        super().__init__(input_file_parser)
        self.image_path = self.user_input_data['style_image']
        self.num_phases = 2
        self.phase_colors = None
        self.image_resizer = self.pick_image_resizer()

    def encode_image(self):
        image = self.load_image(self.image_path)
        self.phase_colors = ipu.compute_phase_color_clusters(self.num_phases, np.asarray(image))
        image = self.image_resizer(image) 
        
        image = kp_image.img_to_array(image)
        image = self.add_batch_dimension_to_image(image)
        self.encoded_image = image
        return self.encoded_image
 

class TwoPhaseGrayscaleMicrostructureEncoder(MicrostructureEncoder):
    """
    This class handles images that have two phases clearly marked via
    pixel intensity value for a 2D image. The encoded image is simply the 
    grayscale of the input image duplicated over all three color channels.
    """
    def __init__(self, input_file_parser):
        super().__init__(input_file_parser)
        self.image_path = self.user_input_data['style_image']
        self.num_phases = 2
        self.phase_colors = None
        self.image_resizer = self.pick_image_resizer()

    def encode_image(self):
        image = self.load_image(self.image_path)
        self.phase_colors = ipu.compute_phase_color_clusters(self.num_phases, np.asarray(image))
        image = self.image_resizer(image) 
        
        image = kp_image.img_to_array(image)
        image = self.add_batch_dimension_to_image(image)
        self.encoded_image = image
        return self.encoded_image
    



class ThreePhaseMicrostructureEncoder(MicrostructureEncoder):
    """
    This class handles images that have three phases clearly marked via
    pixel intensity value for a 2D image.
    """
    def __init__(self, input_file_parser):
        super().__init__(input_file_parser)
        self.image_path = self.user_input_data['style_image']
        self.num_phases = 3
        self.phase_colors = None

    def encode_image(self):
        image = self.load_image(self.image_path)
        self.phase_colors = ipu.compute_phase_color_clusters(self.num_phases, np.asarray(image))
        image = self.image_resizer(image)
        
        image = kp_image.img_to_array(image)
        image = self.add_batch_dimension_to_image(image)
        self.encoded_image = image
        return self.encoded_image
    

