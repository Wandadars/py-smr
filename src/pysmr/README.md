
# This work is inspired by the work of [Insert Nature Paper] for stochastic generation of microstructures.

# Profiling code
Run the code using python -m cProfile -o pysmr.prof \[path\_to\_executable\] \[your\_input\_file\]

You can run the profile file (pysmr.prof) through a visualizer like SnakeViz.


You can also visualize the output using [KCacheGrind](https://julien.danjou.info/guide-to-python-profiling-cprofile-concrete-case-carbonara/
)


python -m cProfile -o myscript.cprof myscript.py

pyprof2calltree -k -i myscript.cprof


# Design Intent

# Software Structure
The classes in use are:

MicroscaleImageReconstructor

ImageOutputGenerator

ModelArchitecture

LossEvaluator

Optimizer

MicrostructureEncoder

MicrostructureDecoder

InputFileParser


The primary goal of this project is to stochastically generate images of microstructures based on an input style image.

##Flow of Information
An input image of a two-phase microstructure distribution is fed into an encoder object (not to be confused with the nomenclature of "encoding" in the context of the VGG19 neural network) that parses the image file. It performs image conversions to ensure the output image that is fed to the main code is in an RGB format with each phase of the input mapped to a particular value of in the RGB color space. This image is referred to as the "style image" in the context of neural style transfer.  An initial image that will become the generated output image is created and initialized with random pixel noise betwen 0-255 in each of the RGB channels.

The style image is passed through the VGG19 neural network. The input data activates different parts of the individual layers that compose the VGG19 architecture. The layers are referred to as "feature maps". The VGG19 architecture uses convolutional layers, which have a 3D structure to them. The feature maps are re-shaped into vector data structures, and then passed through an operation that computes the Gram matrix from the feature vector. The Gram matrix captures what features are present together in a particular layer, and so it serves as a way to capture general correlations of features as they appear. This quantity can be computed for all of the layers of the VGG19 network when the style image is passed through.  The initial generated image is also passed through the VGG19 network, and the gram matrices for each layer are collected.  The difference between the Gram matrices at a particular layer for the style and generated image is computed, and a matrix norm is performed on the result to obtain a scalar value. This value is referred to as the "style loss". The principle at play is that different images with similar features will activate features within a layer in a similar way, and this is what drives the optimization process. 

The optimization process collects the style loss between the style image and the generated image for each layer in the network, and sums them to obtain a single scalar that reprents the total loss. This is the objective function that is to be minimized. The gradient of the error with respect to the pixel values is computed, and gradient descent is used to update the individual pixel values in the generated image such that the total error is reduced. The process of feeding the updated generated image through the VGG19 network, computing the error, computing the gradients, and updating the pixels is repeated until there is sufficient convergence of the total error.

The above methodology covers the simplest form of the style loss. More advanced losses can (and will be) added to improve the statistics of the generated image with respect to the style image. Morphological statistics of the style image can be computed, and corresponding statistics on the generated image can also be computed during each iteration. The difference between the statistics can be added to the total loss function to drive the optimization towards respecting the statistical distribution of the style image as it created the generated image.


