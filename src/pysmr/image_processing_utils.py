import logging
import PIL

import sklearn.cluster as sklcluster
import numpy as np
import scipy as sp
import tensorflow as tf
import tensorflow.contrib.eager as tfe

import global_settings as gs

logger = logging.getLogger('pysmr:' + __name__)


def convert_1_bit_pixel_image_to_rgb(image):
    """
        Converts a 1 bit pixel image to RGB. This is a binary format used by 
        the PIL module.
    """
    if '1' in image.getbands():
        logger.info('Converting 1-bit pixel image to RGB')
        image = image.convert('RGB')
    return image

def convert_8_bit_pixel_image_to_rgb(image):
    """
        Converts an 8 bit pixel image to RGB. This is a binary format used by 
        the PIL module.
    """
    if 'L' in image.getbands():
        logger.info('Converting 8-bit pixel image to RGB')
        image = image.convert('RGB')
    return image

def convert_palettized_image_to_rgb(image):
    """
        Converts a palettized Pillow image object  to RGB if image is
        Palletized, mode='P'
    """
    if 'P' in image.getbands():
        logger.info('Converting Palettized image to RGB')
        image = image.convert('RGB')
    return image

def remove_alpha_channel(image):
    # Only process if image has transparency 
    if image.mode in ('RGBA', 'LA') or (image.mode == 'P' and 'transparency' in image.info):
        logger.debug('Transparency detected in input image. Removing alpha channel.')
        rgb_image = image.convert('RGB')
        return rgb_image
    else:
        return image

def unique_pixel_values(image):
    """
    The image input is handled as a 3d numpy array [H, W, channels].
    """
    unique_pixels = np.unique(image.reshape(-1, image.shape[2]), return_counts = True, axis=0)
    #logger.debug('Unique pixel values: ')
    #logger.debug(unique_pixels)
    return unique_pixels

def compute_phase_color_clusters(number_of_phases, image):
    """
    Uses k-means clustering to determine the number_of_phases most
    likely clusters of colors in an input image.

    Limits color clusters to the range of [0,255]
    
    Inputs:
        number_of_phases: integer number of color clusters to search image for
        image: 3D numpy array of the form [H,W,C]
    """
    logger.info('Estimating Phase color labels')
    logger.debug('Phase color cluster image shape:' + str(image.shape))
    kmeans = sklcluster.KMeans(n_clusters=number_of_phases)
    height, width, channels = image.shape
    flattened_image = image.reshape((height*width, channels))
    kmeans = kmeans.fit(flattened_image)
    labels = kmeans.predict(flattened_image)
    centroids = kmeans.cluster_centers_

    #limit color values of centroids to [0,255]   
    for centroid in centroids:
        for i, channel in enumerate(centroid):
             centroid[i] = max(min(channel, 255), 0.0) 

    phase_colors = {'phase_' + str(i+1): centroid for i, centroid in enumerate(centroids)}
    logger.debug('Estimated Phase Colors:')
    logger.debug(phase_colors)
    return phase_colors

def map_pixels_to_discrete_values(image, color_labels):
    """
    Takes an image[H,W,C] with floating point pixel values and maps each pixel RGB value
    to a new value based on the closest Euclidean distance to one of the RGB sets
    in the color_labels input dictionary.

    Inputs:
        image: 3D numpy array of the form [H,W,C]
        color_labels: Dictionary with data entries ['phase_1': [R1,G1,B1], 'phase_2': [R2,G2,B2]...]

    Outputs:
        3D numpy array of the form [H,W,C] with all pixel values mapped to one of the discrete
        entries provided in the color_labels input

    """
    phase_colors = np.array([color for color in color_labels.values()])

    distances = np.sum((image[:,:,np.newaxis,:] - phase_colors) ** 2, axis=3)
    min_indices = distances.argmin(2)

    mapped_image = phase_colors[min_indices]
    return mapped_image

def lee_filter(image, size):
    img_mean = sp.ndimage.filters.uniform_filter(np.float32(image), (size, size, size))
    img_sqr_mean = sp.ndimage.filters.uniform_filter(np.float32(image)**2, (size, size, size))
    img_variance = img_sqr_mean - img_mean**2

    overall_variance = sp.ndimage.measurements.variance(np.float32(image))

    img_weights = img_variance / (img_variance + overall_variance)
    img_output = img_mean + img_weights * (image - img_mean)
    return img_output


class ImageRegularizer():
    """
    A class that is responsible for performing image adjustments to
    an input image. Things like blurring, etc. This is what is meant by
    "regularizing" an image.
    """
    def __init__(self):
        self.gaussian_blur = GaussianBlur(3,2)

    def regularize_image(self, image):
        """
        Performs the sequence of regularization steps. The input image is cast to the float32
        data type for the method to properly exectute, the 32bit results are then cast back to
        the data type that the user set for the network. The casting to 32 bit is redundant if
        the user specified the input to be a 32bit image.

        Inputs:
            image: A 4D tensor representing an image [Batch, Height, Width, Channels]
                   of potential float types of float32 or float16

        Outputs:
            A version of the input tensor passed through many filters. 
        """
        regularized_image = self.gaussian_blur.adjust_image(tf.cast(image, tf.float32))
        return tf.cast(regularized_image, gs._FLOAT_DTYPE)


class GaussianBlur():
    def __init__(self, kernel_size, standard_dev):
        self.kernel_size = int(kernel_size)
        self.standard_dev = float(standard_dev)
    
    def adjust_image(self, image):
        return self.gaussian_blur(image, self.kernel_size, self.standard_dev)

    def gaussian_kernel(self, kernel_size, standard_dev):
        """Makes 2D gaussian Kernel for convolution."""
        distribution = tf.distributions.Normal(0.0, standard_dev)
        vals = distribution.prob(tf.range(start=-kernel_size, limit=kernel_size+1, dtype=tf.float32))
        gauss_kernel = tf.einsum('i,j->ij', vals, vals)
        return gauss_kernel / tf.reduce_sum(gauss_kernel)

    def gaussian_blur(self, image, kernel_size, standard_dev):
        """
        Uses a Gaussian kernel to perform a 2D convolution on each of the input
        image's channels.

        Inputs:
            image: A 4D Tensorflow tensor representing an image [Batch, Height, Width, Channels]
            kernel_size: The 2D dimension(square) of the kernel to be used for the Gaussian blur convolution
            standard_dev: The standard deviation parameter used to evaluate the Gaussian equation for the kernel

        Outputs:
            A version of the input tensor passed through a 2D gaussian filter convolution.
        
        """
        gauss_kernel_2d = self.gaussian_kernel(kernel_size, standard_dev)
        gauss_kernel = tf.tile(gauss_kernel_2d[:, :, tf.newaxis, tf.newaxis], [1, 1, 3, 1]) # 5*5*3*1
        # Pointwise filter that does nothing
        pointwise_filter = tf.eye(3, batch_shape=[1, 1])
        output = tf.nn.separable_conv2d(image, gauss_kernel, pointwise_filter, strides=[1, 1, 1, 1], padding='SAME')
        return output




class InitialImageGenerator:
    def __init__(self, user_input_data, base_image):
        self.user_input_data = user_input_data
        self.base_image = base_image

    def create_image(self):
        raise NotImplementedError
    

class RandomThreeChannelNoiseImageGenerator(InitialImageGenerator):
    def __init__(self, user_input_data, base_image):
        super().__init__(user_input_data, base_image)

    def create_image(self):
        """
        Generate initial noisy image with random noise [0,255] in each of the three color channels [R,G,B].
        """
        batch, height, width, channels = self.base_image.shape #Use input image dimensions
        if 'output_height' in self.user_input_data and 'output_width' in self.user_input_data:
            logger.info('Reading initial generated output image dimensions from input file')
            try:
                height = int(self.user_input_data['output_height'])
                width = int(self.user_input_data['output_width'])
            except:
                #Fall back on input image dimensions
                logger.warning('No user-defined output image dimensions provided as input. Using input image dimensions for output image size.')
        else:
            logger.info('Output image dimensions set to input image dimensions')

        if 'random_seed' in self.user_input_data:
            random_seed = int(self.user_input_data['random_seed'])
            np.random.seed(random_seed)
        
        init_image = self.generate_random_noise_image(height, width, channels)
        logger.debug('Generated Noise image shape: ' + str(init_image.shape))
        
        init_image = tfe.Variable(init_image, dtype=gs._FLOAT_DTYPE)
        return init_image

    def generate_random_noise_image(self, height, width, channels):
        """
        Generates a noisy image by adding random noise to the image
        """
        noisy_image = np.random.uniform(0, 255, (1, height, width, channels)).astype('float32')
        noisy_image = tf.keras.applications.vgg19.preprocess_input(noisy_image)
        return noisy_image


class RandomOneChannelNoiseImageGenerator(InitialImageGenerator):
    def __init__(self, user_input_data, base_image):
        super().__init__(user_input_data, base_image)

    def create_image(self):
        """
        Generate initial noisy image
        """
        batch, height, width, channels = self.base_image.shape #Use input image dimensions
        try:
            height = int(self.user_input_data['output_height'])
            width = int(self.user_input_data['output_width'])
        except:
            #Fall back on input image dimensions
            logger.warning('No user-defined output image dimensions provided as input. Using input image dimensions for output image size.')

        if 'random_seed' in self.user_input_data:
            random_seed = int(self.user_input_data['random_seed'])
            np.random.seed(random_seed)
        
        init_image = self.generate_random_noise_image(height, width, channels)
        logger.debug('Generated Noise image shape: ' + str(init_image.shape))
        
        init_image = tfe.Variable(init_image, dtype=tf.float32)
        return init_image

    def generate_random_noise_image(self, height, width, channels):
        """
        Generates a noisy image by adding random noise to the image
        """
        noisy_image = np.ones((1, height, width, channels)).astype('float32')
        noise = np.random.uniform(0, 255, (1, height, width, 1)).astype('float32')
        noisy_image = noisy_image * noise
        noisy_image = tf.keras.applications.vgg19.preprocess_input(noisy_image)
        return noisy_image

