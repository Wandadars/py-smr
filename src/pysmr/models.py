import logging

import numpy as np
import tensorflow as tf

logger = logging.getLogger('pysmr:' + __name__)


class ModelArchitecture:
    def __init__(self, input_file_parser):
        self.user_input_data = input_file_parser.user_input_data
        self.model = None

    def create_model(self):
        raise NotImplementedError

    def get_model(self):
        return self.model
    
    def get_feature_representations(self, image):
        """
        Computes style feature representation.

        Loads and preprocesses the style image and feeds it
        through the network to obtain the outputs of the
        intermediate layers.

        Args:
            image: Numpy 4D image tensor (batch, height, width, channels).
            For-Future:: image: Tensorflow 4D image tensor (batch, height, width, channels).

        Returns:
            A list of style features for each layer.
        """
        # compute style features
        style_outputs = self.model(image)
        logger.debug('Style outputs shape:')
        logger.debug(style_outputs[0].shape)
        # Get the style feature representations from model
        style_features = [style_layer[0] for style_layer in style_outputs]
        return style_features


class VGG19Model(ModelArchitecture):
    def __init__(self, input_file_parser):
        super().__init__(input_file_parser)
        self.style_layers = None
        self.compute_layer_weights = self.weight_selector()

    def weight_selector(self):
        if 'layer_weights' in self.user_input_data:
            if self.user_input_data['layer_weights'].lower() == 'novak':
                layer_weights_computor = self.novak_weights
            elif self.user_input_data['layer_weights'].lower() == 'linear':
                layer_weights_computor = self.linear_weights
            elif self.user_input_data['layer_weights'].lower() == 'uniform':
                layer_weights_computor = self.uniform_weights
        else:
            layer_weights_computor = self.no_weights_update
        return layer_weights_computor
    
    def no_weights_update(self):
        logger.info('Leaving layer weights hardcoded')
        pass

    def novak_weights(self):
        logger.info('Using Novak layer weights')
        try:
            weight_decay_factor = float(self.user_input_data['weight_decay_factor'])
        except KeyError:
            logger.warning('weight_decay_factor not specified. Default value 2.0 used. ')
            weight_decay_factor = 2.0

        num_layers = len(self.style_layers)
        for layer_loc, layer in enumerate(self.style_layers):
            layer['layer_weight'] = weight_decay_factor ** (num_layers - layer_loc)
    
    def linear_weights(self):
        """
        The first layer has a weight of 2, the last has a weight of 1, and between them
        is a linear variation.
        """
        logger.info('Using linear layer weights')
        for layer_loc, layer in enumerate(self.style_layers):
            layer['layer_weight'] = ((2.0 - 1.0) / len(self.style_layers)) * layer_loc + 2.0

    def uniform_weights(self):
        """
        Every layer has the same weight value of one. 
        """
        logger.info('Using uniform layer weights')
        for layer_loc, layer in enumerate(self.style_layers):
            layer['layer_weight'] = 1.0 
    
    def normalize_weights(self):
        """
        Argument:
        style_layers -- List of dictionaries that contain the keys "layer_name" and "layer_weight"
        """
        weights = self.style_layer_weights()
        weight_sum = sum(weights)
        if weight_sum > 0:
            for layer_data in self.style_layers:
                layer_data['layer_weight'] = layer_data['layer_weight'] / weight_sum
        else:
            for layer_data in self.style_layers:
                layer_data['layer_weight'] = 0

        logger.debug('Normalized Layer Weights (in no particular order)')
        for layer in self.style_layers:
            logger.debug(layer['layer_name'] + ': ' + str(layer['layer_weight']))

    def preprocess_image(self, image):
        """
        Gets image and performs the preprocessing as is expected according
        to the VGG training process.
        VGG networks are trained on image with each channel normalized by
        mean = [103.939, 116.779, 123.68] with channels BGR.

        Args:
            image: 3D numpy array representing an RGB image
        
        Returns:
            The image with the VGG19 mean channel values subtracted
            and the channels flipped to BGR.

        """
        logger.debug('Preprocessed image shape:')
        logger.debug(image.shape)

        #Default preprocessing is mode='caffe' which converts from RGB to
        #BGR then zero-centers each channel w.r.t. the ImageNet dataset and
        #does no rescaling
        image = tf.keras.applications.vgg19.preprocess_input(image)
        return image

    def deprocess_image(self, processed_img):
        """
        To view the output of the optimization, and inverse preprocessing step is required.
        Args:
            processed_img: A tensor holding the optimized image in the Keras form with the channels revsered
                           and the mean values of the VGG dataset subtracted off the channels, also it has
                           a batch dimension on it (4D).
        Returns:
            A tensor with the batch dimension removed, the VGG19 RGB values added back onto the channels,
            and the channels placed back into RGB order
        """
        image = processed_img.copy()
        if len(image.shape) == 4:
            image = np.squeeze(image, 0)
            assert len(image.shape) == 3, ("Input to deprocess image must be an image of dimension [1, height, width, channel] or [height, width, channel]")
        if len(image.shape) != 3:
            raise ValueError("Invalid input to deprocessing image")

        # perform the inverse of the preprocessing step
        VGG_MEAN = [103.939, 116.779, 123.68]  #BGR
        image[:, :, 0] += VGG_MEAN[0]
        image[:, :, 1] += VGG_MEAN[1]
        image[:, :, 2] += VGG_MEAN[2]
        image = image[:, :, ::-1] #Flip back to RGB order
        return image

    def create_model(self):
        """
        Creates a model with access to intermediate layers. 

        This function loads the VGG19 model and accesses the intermediate layers. 
        These layers will then be used to create a new model that will take input image
        and return the outputs from these intermediate layers from the VGG model. 

        Returns:
            A Keras model that takes image inputs and outputs the style and 
            content intermediate layers. 
        """
        # Load model. Load pretrained VGG, trained on imagenet data
        if 'float_mode' in self.user_input_data:
            if self.user_input_data['float_mode'].lower() == 'float16':
                logger.info('Setting float mode to float16')
                self.use_16_bit_model()

        vgg = tf.keras.applications.vgg19.VGG19(include_top=False, weights='imagenet')
        vgg.trainable = False

        # Get output layers corresponding to style layers 
        style_outputs = [vgg.get_layer(name).output for name in [layer_data['layer_name'] for layer_data in self.style_layers]]
        model_outputs = style_outputs 
        
        model = tf.keras.models.Model(vgg.input, model_outputs) # Build model

        # Don't need to train any layers of the model 
        for layer in model.layers:
            layer.trainable = False
        logger.debug('Model Summary')
        model.summary()
        self.model = model

    def use_16_bit_model(self):
        tf.keras.backend.set_floatx('float16')
        tf.keras.backend.set_epsilon(1e-4)

    def style_layer_weights(self):
        return [layer_data['layer_weight'] for layer_data in self.style_layers]


class VGG19Model1(VGG19Model):
    def __init__(self, input_file_parser):    
        super().__init__(input_file_parser)
        self.style_layers = [{'layer_name': 'block1_conv1', 'layer_weight': 0.7},
                             {'layer_name': 'block1_conv2', 'layer_weight': 0.7},
                             {'layer_name': 'block1_pool', 'layer_weight': 0.7},
                             {'layer_name': 'block2_conv1', 'layer_weight': 0.7},
                             {'layer_name': 'block2_conv2', 'layer_weight': 0.7},
                             {'layer_name': 'block2_pool', 'layer_weight': 0.7},
                             {'layer_name': 'block3_conv1', 'layer_weight': 0.7},
                             {'layer_name': 'block3_conv2', 'layer_weight': 0.7},
                             {'layer_name': 'block3_conv3', 'layer_weight': 0.4},
                             {'layer_name': 'block3_conv4', 'layer_weight': 0.4},
                             {'layer_name': 'block3_pool', 'layer_weight': 0.4},
                             {'layer_name': 'block4_conv1', 'layer_weight': 0.4},
                             {'layer_name': 'block4_conv2', 'layer_weight': 0.3},
                             {'layer_name': 'block4_conv3', 'layer_weight': 0.3},
                             {'layer_name': 'block4_conv4', 'layer_weight': 0.3},
                             {'layer_name': 'block4_pool', 'layer_weight': 0.3},
                             {'layer_name': 'block5_conv4', 'layer_weight': 0.2},
                             {'layer_name': 'block5_conv4', 'layer_weight': 0.2},
                             {'layer_name': 'block5_conv4', 'layer_weight': 0.2},
                             {'layer_name': 'block5_conv4', 'layer_weight': 0.2},
                             {'layer_name': 'block5_pool', 'layer_weight': 0.2},
                            ]
        self.compute_layer_weights()
        self.normalize_weights()


class VGG19Model2(VGG19Model):
    """
    All convolutional layers except pooling
    """
    def __init__(self, input_file_parser):    
        super().__init__(input_file_parser)
        self.style_layers = [{'layer_name': 'block1_conv1', 'layer_weight': 0.7},
                             {'layer_name': 'block1_conv2', 'layer_weight': 0.7},
                             {'layer_name': 'block2_conv1', 'layer_weight': 0.7},
                             {'layer_name': 'block2_conv2', 'layer_weight': 0.7},
                             {'layer_name': 'block3_conv1', 'layer_weight': 0.7},
                             {'layer_name': 'block3_conv2', 'layer_weight': 0.7},
                             {'layer_name': 'block3_conv3', 'layer_weight': 0.4},
                             {'layer_name': 'block3_conv4', 'layer_weight': 0.4},
                             {'layer_name': 'block4_conv1', 'layer_weight': 0.4},
                             {'layer_name': 'block4_conv2', 'layer_weight': 0.3},
                             {'layer_name': 'block4_conv3', 'layer_weight': 0.3},
                             {'layer_name': 'block4_conv4', 'layer_weight': 0.3},
                             {'layer_name': 'block5_conv4', 'layer_weight': 0.2},
                             {'layer_name': 'block5_conv4', 'layer_weight': 0.2},
                             {'layer_name': 'block5_conv4', 'layer_weight': 0.2},
                             {'layer_name': 'block5_conv4', 'layer_weight': 0.2},
                            ]
        self.compute_layer_weights()
        self.normalize_weights()


class VGG19Model3(VGG19Model):
    """
    Only the final convolutional layers in each block.
    """
    def __init__(self, input_file_parser):    
        super().__init__(input_file_parser)
        self.style_layers = [{'layer_name': 'block1_conv2', 'layer_weight': 0.1},
                             {'layer_name': 'block2_conv2', 'layer_weight': 0.1},
                             {'layer_name': 'block3_conv4', 'layer_weight': 0.1},
                             {'layer_name': 'block4_conv4', 'layer_weight': 0.1},
                             {'layer_name': 'block5_conv4', 'layer_weight': 0.1},
                            ]
        self.compute_layer_weights()
        self.normalize_weights()

class VGG19Model4(VGG19Model):
    """
    Layers used in the Scientific Reports paper from the Nature journal.
    """
    def __init__(self, input_file_parser):    
        super().__init__(input_file_parser)
        self.style_layers = [{'layer_name': 'block1_conv1', 'layer_weight': 1},
                             {'layer_name': 'block1_pool', 'layer_weight': 1},
                             {'layer_name': 'block2_pool', 'layer_weight': 1},
                             {'layer_name': 'block3_pool', 'layer_weight': 1},
                             {'layer_name': 'block4_pool', 'layer_weight': 1},
                             {'layer_name': 'block5_pool', 'layer_weight': 0},
                            ]
        self.compute_layer_weights()
        self.normalize_weights()




