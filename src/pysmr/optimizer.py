import logging

import scipy.optimize as spo 
import tensorflow as tf
import tensorflow_probability as tfp

import image_processing_utils as ipu

logger = logging.getLogger('pysmr:' + __name__)


class Optimizer:
    def __init__(self, input_file_parser, loss_evaluator):
        self.user_input_data = input_file_parser.user_input_data
        self.loss_evaluator = loss_evaluator
        self.optimizer = None

    def minimize(self, style_transfer_state):
        raise NotImplementedError

    def get_loss(self):
        raise NotImplementedError

    @tf.function
    def compute_grads_and_losses(self, style_transfer_state):
        """
        Computes gradients with respect to input image

        Input:
            style_transfer_state: Dictionary containint state information about the
                                  image being optimized(generated) and other relevant
                                  quantities that are used for the loss generation.

            Return:
            (grads, loss): A tuple of the gradients of the loss with respect to the input image,
                           and a dictionary containing the individial loss components and the total loss.
        """
        with tf.GradientTape() as tape:
            tape.watch(style_transfer_state['image'])
            loss = self.loss_evaluator.compute_total_loss(style_transfer_state)
        total_loss = loss['total_loss']
        grads = tape.gradient(total_loss, style_transfer_state['image'])
        return grads, loss
    
    def filter_gradients(self, grads):
        """
        Performs an adjustment of the gradient tensor to try and
        reduce the oscillations and/or the noisy nature of the gradients
        that are computed in an effort to reduce checkerboard patterning
        in the optimized output image.

        Inputs:
            grads: A 4D TensorFlow tensor that represents the gradients for updating an image's pixels.
                   Dimensions are [Batch, Height, Width, Channels].

        Outpus:
            filtered_grads: The same dimension of the input with the values filtered to
                            adjust the statistics of the gradients.
        """
        gaussian_blurer = ipu.GaussianBlur(3,1)
        filtered_gradients = gaussian_blurer.adjust_image(grads)
        return filtered_gradients

    def gradients_statistics(self, grads):
        mean = tf.reduce_mean(grads)
        variance = tf.reduce_variance(grads)
        max_val = tf.reduce_max(grads)
        min_val = tf.reduce_min(grads)
        gradient_statistics = {'mean': mean, 'variance': variance,
                               'max': max_val, 'min': min_val}
        return gradient_statistics


class AdamOptimizer(Optimizer):
    def __init__(self, input_file_parser, loss_evaluator):
        super().__init__(input_file_parser, loss_evaluator)
        self.grads = None
        self.losses = None

        try:
            learning_rate = float(self.user_input_data['learning_rate'])
        except KeyError:
            logger.warning('learning_rate not in input file. Default set to 3')
            learning_rate = 3
        self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate) 

    def minimize(self, style_transfer_state):
        self.grads, self.losses = self.compute_grads_and_losses(style_transfer_state)
        self.optimizer.apply_gradients([(self.grads, style_transfer_state['image'])])
    
    def get_loss(self):
        return self.losses

class AdamOptimizerGradientFiltering(Optimizer):
    def __init__(self, input_file_parser, loss_evaluator):
        super().__init__(input_file_parser, loss_evaluator)
        self.grads = None
        self.losses = None

        try:
            learning_rate = float(self.user_input_data['learning_rate'])
        except KeyError:
            logger.warning('learning_rate not in input file. Default set to 3')
            learning_rate = 3
        self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate) 

    def minimize(self, style_transfer_state):
        self.grads, self.losses = self.compute_grads_and_losses(style_transfer_state)
        self.grads = self.filter_gradients(grads)
        self.losses = losses
        self.optimizer.apply_gradients([(self.grads, style_transfer_state['image'])])
        logger.debug(self.gradients_statistics(self.grads))
    
    def get_loss(self):
        return self.losses




class LBFGSOptimizer(Optimizer):
    """
    The LBFGS optimization process is different from the ADAM process because it performs the iterations internally
    within the algorithm, such that the output represents the final converged value instead of an intermediate state on
    the path towards convergence.
    """
    def __init__(self, input_file_parser, loss_evaluator):
        super().__init__(input_file_parser, loss_evaluator)
        self.style_transfer_state = None
        self.optimized_image_shape = None

    def minimize(self, style_transfer_state):
        self.style_transfer_state = style_transfer_state
        
        #image_to_optimize = tf.Variable(style_transfer_state['image'].initialized_value())
        image_to_optimize = style_transfer_state['image']
        self.optimized_image_shape = image_to_optimize.shape
        flattened_image = tf.reshape(image_to_optimize,[-1])
        logger.debug('Calling LBFGS Optimizer')
        optim_results = tfp.optimizer.lbfgs_minimize(self.compute_grads_and_losses, initial_position=flattened_image, num_correction_pairs=10, f_relative_tolerance=1e-6,max_iterations=1000)
        self.losses['total_loss'] = optim_results.objective_value

        optimized_image = tf.reshape(optim_results.position,self.optimized_image_shape)
        style_transfer_state['image'] = tf.Variable(optimized_image)
        logger.debug('Finished LBFGS optimization')

    #@tf.function
    def compute_grads_and_losses(self, image):
        """
        Computes gradients and losses with respect to input image
        
        Inputs:
            image: A 4D TensorFlow tensor that represents the image being optimized.
                   Dimensions are [Batch, Height, Width, Channels].

        Outpus:
            A tuple consisting of a dictionary of different losses that were computed using in the input image, and
            a 4D TensorFlow tensor that contains the gradients of the total loss with respect to each pixel of the
            input image. Dimensions are [batch, height, width, channels].
        """
        reshaped_image = tf.reshape(image,self.optimized_image_shape) #return to original shape
        
        style_transfer_state = {'image': reshaped_image, 'style_image_features': self.style_transfer_state['style_image_features']}
        with tf.GradientTape() as tape:
            tape.watch(style_transfer_state['image'])
            loss = self.loss_evaluator.compute_total_loss(style_transfer_state)
        total_loss = loss['total_loss']
        grads = tape.gradient(total_loss, style_transfer_state['image'])
        self.losses = loss
        self.grads = grads
        logger.debug('Total Loss')
        logger.debug(total_loss)
        flattened_grads = tf.reshape(grads, [-1])
        return loss['total_loss'], flattened_grads

    def get_loss(self):
        return self.losses


