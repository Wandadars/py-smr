"""
Provides capabilities for parsing an input file where comments are ignored and key-value pairs
are separated by spaces
"""
import logging
import os
import re

logger = logging.getLogger(__name__)


class InputFileParser(object):
    def __init__(self, file_name):
        self.user_input_data = {}
        self.input_file_name = ''
        self.read_input_file(file_name)

    def read_input_file(self, file_name):
        try:
            with open(file_name):
                pass
        except IOError:
            logger.error('Error: the file ' + file_name + ' could not be found at:\n' + os.path.dirname(os.path.realpath(__file__)))
            raise IOError

        self.input_file_name = file_name
        logger.info('Reading Input file: ' + self.input_file_name)
        file_data = self.clean_input_file_data(self.input_file_name)
        self.parse_input_file_options(file_data)

    def parse_input_file_options(self, file_data):
        logger.info('Following Inputs Read')
        for line in file_data:
            li = line.strip().rstrip()
            if li:
                li = li.split(' ', 1)
                logger.debug(li)
                key = li[0].strip()
                value = li[1].strip()
                self.user_input_data[key] = value

    def clean_input_file_data(self, file_name):
        """
        Remove comments and tab characters from the input file data 
        
        """
        with open(file_name) as f:
            file_data = f.readlines()
        logger.debug(type(file_data))
        file_data = [re.sub(r'\s*#.*', '', line) for line in file_data]  # Remove comments
        file_data = [line.replace('\t', ' ') for line in file_data] #Remove tabs
        return file_data

    @staticmethod
    def print_valid_keywords():
        logger.info('The following keywords can be used in the input file:')
        logger.info('input_image        =  Name of the input image to be used as the morphological style control for the generated output (str)')
        logger.info('output_prefix        =  Prefix name that will be assigned to the generated image (str)')
        logger.info('num_iterations        =  Name of the input image to be used as the morphological style control for the generated output (str)')
        logger.info('model_type         =  Type of trained model to use for the reconstruction of a microstructure')
        logger.info('characterizations  =  Comma separated list of which characterizations to apply to the input image for use in the reconstruction.')


