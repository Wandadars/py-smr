import time
import functools
import logging

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['figure.figsize'] = (10,10)
mpl.rcParams['axes.grid'] = False
import os

import tensorflow as tf

#For tensorflow 2.0
#tf.config.gpu.set_per_process_memory_fraction(0.75)
#tf.config.gpu.set_per_process_memory_growth(True)

import encoder
import decoder
import image_processing_utils as ipu
import losses
import global_settings as gs

logging.getLogger('matplotlib').setLevel(logging.WARNING)
logger = logging.getLogger('pysmr:' + __name__)


class MicroscaleImageReconstructor:
    def __init__(self, input_file_parser):
        self.user_input_data = input_file_parser.user_input_data
        self.style_path = self.user_input_data['style_image']
        
        logger.info('Running Tensorflow version: ' + str(tf.version.VERSION))

    def create_reconstruction(self):
        raise NotImplementedError


class TensorFlowMicroscaleImageReconstructor(MicroscaleImageReconstructor):
    def __init__(self, input_file_parser, initial_state):
        super().__init__(input_file_parser)
        self.image_encoder = initial_state['encoder'] 
        self.image_decoder = initial_state['decoder'] 
        self.model = initial_state['model']
        self.timestepper = initial_state['timestepper']
        self.style_image = None

    def create_reconstruction(self):
        """
        Implementation based on the work at:
        https://github.com/tensorflow/models/blob/master/research/nst_blogpost/4_Neural_Style_Transfer_with_Eager_Execution.ipynb
        """
        logger.info('Setting Tensorflow eager execution')
        self.set_eager_execution()
        
        logger.info('Encoding image')
        self.image_encoder.encode_image()
        self.image_decoder.set_phase_colors(self.image_encoder.phase_colors) #Provide decoder the phase color map for input image

        logger.info('Creating Model')
        self.model.create_model()  
        
        #Store encoded style image with VGG19 preprocessing
        logger.info('Creating Style Image')
        self.style_image = self.model.preprocess_image(self.image_encoder.encoded_image)

        logger.info('Running Style Transfer')
        self.run_style_transfer()
        
        logger.info('Program complete')
        

    def set_eager_execution(self):
        opts = tf.GPUOptions(allow_growth=True)
        tf.enable_eager_execution(config=tf.ConfigProto(gpu_options=opts))
        tf.executing_eagerly()
        logger.info("Eager execution: {}".format(tf.executing_eagerly()))
 
    def create_configuration_dict(self):
        """
        Creates the dictionary data structure that is used to pass information
        to the gradient calcualtor.
        """
        logger.info('Style image shape: ' + str(self.style_image.shape))
        image_generator = ipu.RandomThreeChannelNoiseImageGenerator(self.user_input_data, self.style_image)
        init_image = image_generator.create_image()
        style_features = self.model.get_feature_representations(tf.cast(self.style_image, gs._FLOAT_DTYPE)) # from intermediate layers
        config = {
          'image': init_image,
          'style_image_features': style_features,
          'raw_image': None}
        return config

    def run_style_transfer(self):
        """
        Performs the high-level style transfer process.
        """
        config = self.create_configuration_dict()
        global_start = time.time()
        self.timestepper.iterate(config)
        logger.info('Total time: {:.4f}s'.format(time.time() - global_start))


class TimeStepper():
    def __init__(self, input_file_parser, model, optimizer, image_decoder):
        self.user_input_data = input_file_parser.user_input_data
        self.model = model
        self.optimizer = optimizer
        self.image_decoder = image_decoder
        self.image_regularizer = ipu.ImageRegularizer()
        self.output_generator = VGG19ImageOutputGenerator(self.user_input_data)
        
        self.output_freq = None
        self.regularization_freq = None
        self.num_iterations = None
    
    def initialize_timestepping_parameters(self):
        try:
            num_iterations = int(self.user_input_data['num_iterations'])
        except KeyError:
            raise KeyError('num_iterations not specified in input file') 
        
        try:
            output_freq = int(self.user_input_data['output_freq'])
        except KeyError:
            output_freq = 10
            logger.warning('output_freq not specified in input file. Default value set to 10') 
        
        try:
            regularization_freq = int(self.user_input_data['regularization_freq'])
        except KeyError:
            regularization_freq = 1
            logger.warning('regularization_freq not specified in input file. Default value set to 1') 
        
        try:
            regularization_freq = int(self.user_input_data['regularization_freq'])
        except KeyError:
            regularization_freq = 1
            logger.warning('regularization_freq not specified in input file. Default value set to 1') 
    
        return {'num_iterations': num_iterations, 'output_freq': output_freq, 'regularization_freq': regularization_freq}

    def clip_image(self, input_image):
        norm_means = np.array([103.939, 116.779, 123.68]) #BGR mean imagenet data
        min_vals = -norm_means
        max_vals = 255 - norm_means   
        return tf.clip_by_value(input_image, min_vals, max_vals)

    def print_iteration_progress(self):
        """
        To be implemented by the derived classes for whatever output they feel necessary
        """
        raise(NotImplementedError)


class ADAMTimeStepper(TimeStepper):
    def __init__(self, input_file_parser, model, optimizer, image_decoder):
        super().__init__(input_file_parser, model, optimizer, image_decoder)

    def iterate(self, config):
        init_params = self.initialize_timestepping_parameters()
        self.num_iterations = init_params['num_iterations']
        self.output_freq = init_params['output_freq']
        self.regularization_freq = init_params['regularization_freq']
        
        for i in range(self.num_iterations):
            self.take_step(config, i)

    def take_step(self, config, iteration_num):
        iter_start_time = time.time()
        self.optimizer.minimize(config)
        losses = self.optimizer.get_loss()
        clipped = self.clip_image(config['image'])
        config['image'].assign(clipped)
        
        raw_loss_image = self.model.deprocess_image(config['image'].numpy())
        config['raw_image'] = raw_loss_image
        
        if iteration_num % self.regularization_freq == 0 and iteration_num > 0:
            regularize = True
            if 'regularize_once' in self.user_input_data and iteration_num != self.regularization_freq:
                regularize = False
                
            if regularize == True:
                logger.info('Regularizing generated image')
                regularized_image = self.image_regularizer.regularize_image(config['image'])
                config['image'].assign(regularized_image)
        
        if iteration_num % self.output_freq == 0:
            loss_image = self.image_decoder.decode_image(raw_loss_image) 
            self.output_generator.intermediate_output(iteration_num, raw_loss_image, loss_image)
        
        self.print_iteration_progress(iteration_num, losses, time.time() - iter_start_time)
    
    def print_iteration_progress(self, iteration, losses, delta_t):
        style_loss = losses['style_loss']
        total_var_loss = losses['total_variation_loss']
        total_loss = losses['total_loss']
        iteration_string = 'Iter: {:}\tTotal loss: {:.4e}\tStyle loss: {:.4e}\tTV loss: {:.4e}\tTime: {:.4g}s'
        logger.info(iteration_string.format(iteration, total_loss, style_loss, total_var_loss, delta_t))


class LBFGSTimeStepper(TimeStepper):
    def __init__(self):
        super().__init__()

    def iterate(self, config, num_iterations):
        for i in range(num_iterations):
            self.take_step(config, i)
        return config

    def take_step(self, config, iteration_num):
        iter_start_time = time.time()
        self.optimizer.minimize(config)
        losses = self.optimizer.get_loss()
        clipped = self.clip_image(config['image'])
        config['image'].assign(clipped)
        
        raw_loss_image = self.model.deprocess_image(config['image'].numpy())
        config['raw_image'] = raw_loss_image
        
        if i % regularization_freq == 0 and i > 0:
            logger.info('Regularizing generated image')
            regularized_image = self.image_regularizer.regularize_image(config['image'])
            config['image'].assign(regularized_image)
        
        if i % output_freq == 0:
            loss_image = self.image_decoder.decode_image(raw_loss_image) 
            self.output_generator.intermediate_output(iteration_num, raw_loss_image, loss_image)
        
        self.print_iteration_progress(iteration_num, losses, time.time() - iter_start_time)



class ImageOutputGenerator():
    def __init__(self, user_input_data):
        self.user_input_data = user_input_data

    def output_results(self, loss_history):
        raise NotImplementedError

    def create_output_directory(self):
        current_dir = os.getcwd()
        output_dir = current_dir + '/output'
        try:
            os.makedirs(output_dir)
        except FileExistsError:
            # directory already exists
            pass
        logger.info('Output directory is: ' + output_dir)
        self.output_path = output_dir


class VGG19ImageOutputGenerator(ImageOutputGenerator):
    def __init__(self, user_input_data):
        super().__init__(user_input_data)
        self.output_path = None
        self.create_output_directory()

    def intermediate_output(self, iteration_num, raw_image, decoded_image):
        fig, ax = plt.subplots(frameon=False)
        ax.set_axis_off()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        ax.imshow(raw_image.astype('uint8'))
        output_name = 'raw_image_' + str(iteration_num) + '.png'
        output_path = self.output_path + '/' + output_name
        plt.imsave(output_path, raw_image.astype('uint8'))
        plt.close(fig)
        
        fig, ax = plt.subplots(frameon=False)
        ax.set_axis_off()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        ax.imshow(decoded_image)
        output_name = 'decoded_image_' + str(iteration_num) + '.png'
        plt.imsave(self.output_path + '/' + output_name, decoded_image)
        plt.close(fig)
    
    def imshow(self, image):
        out = np.squeeze(image, axis=0) # Remove the batch dimension
        out = out.astype('uint8') # Normalize for display
        return out



