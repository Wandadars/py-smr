import logging
import sys

import tensorflow as tf 

import encoder
import decoder
import losses
import microstructure_reconstructor as mr
import models
import optimizer
import input_parser as ip
import global_settings as gs

logger = logging.getLogger(__name__)


class Main:
    """
    Main class for entry into the py-smr tool. Also handles the setup of the logging capabilities.
    """
    def __init__(self, input_file_name):
        self.setup_logger()
        self.input_parser = ip.InputFileParser(input_file_name)

    def setup_logger(self):
        """
        Creates logging handler and sets up logging output format.
        """
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)

        logging.basicConfig(format='%(asctime)s %(name)-12s %(levelname)-8s: %(message)s', 
                            datefmt='%m/%d/%Y %H:%M %p', filename='pysmr.log', filemode='w',
                            level=logging.DEBUG)

        #For console output from logging
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)
        logging.info('Logger setup complete')
        
    def set_floating_point_type(self):
        try:
            float_type = self.input_parser.user_input_data['float_mode'].lower()
        except KeyError as e:
            logging.info('No float_mode found in input file. Defaulting to float32 floating point values')
            float_type = 'float32'

        if float_type not in {'float16', 'float32'}:
            raise ValueError('Unknown float type specified: ' + str(float_type))
        
        if float_type == 'float16':
            gs._FLOAT_DTYPE = tf.float16
        elif float_type == 'float32':
            gs._FLOAT_DTYPE = tf.float32


    def run(self):
        logging.info('Creating Initial state from factory')
        initial_state = self.run_factory()
        
        logging.info('Setting floating point type for network')
        self.set_floating_point_type()

        logging.info('Initializing Reconstructor')
        self.reconstructor = mr.TensorFlowMicroscaleImageReconstructor(self.input_parser, initial_state)
        
        logging.info('Performing Reconstruction')
        self.reconstructor.create_reconstruction()
        logging.info('Program Finished')

    def run_factory(self):
        user_input_data = self.input_parser.user_input_data
        initial_state = {}

        #Model Type
        if 'model' in user_input_data:
            if user_input_data['model'].lower() == 'vgg19model1':
                model = models.VGG19Model1(self.input_parser)
            if user_input_data['model'].lower() == 'vgg19model2':
                model = models.VGG19Model2(self.input_parser)
            if user_input_data['model'].lower() == 'vgg19model3':
                model = models.VGG19Model3(self.input_parser)
            if user_input_data['model'].lower() == 'vgg19model4':
                model = models.VGG19Model4(self.input_parser)
        else:
            model = models.VGG19Model1(self.input_parser) #Default to full VGG19 network model
        initial_state['model'] = model

        #Encoder type
        if user_input_data['encoder'].lower() == 'two_phase':
            image_encoder = encoder.TwoPhaseMicrostructureEncoder(self.input_parser)
        initial_state['encoder'] = image_encoder

        #Decoder type
        if user_input_data['decoder'].lower() == 'two_phase':
            image_decoder = decoder.TwoPhaseMicrostructureDecoder(self.input_parser)
        if user_input_data['decoder'].lower() == 'two_phase_no_filter':
            image_decoder = decoder.TwoPhaseMicrostructureDecoderNoDespeckling(self.input_parser)
        initial_state['decoder'] = image_decoder
        
        #Losses
        if user_input_data['losses'].lower() == 'hmx_loss':
            loss = losses.HMXLossEvaluator(self.input_parser, initial_state['model'], initial_state['decoder'])
        if user_input_data['losses'].lower() == 'inter_layer_loss':
            loss = losses.InterLayerLossEvaluator(self.input_parser, initial_state['model'], initial_state['decoder'])
        if user_input_data['losses'].lower() == 'hmx_loss_volume_fraction':
            loss = losses.HMXLossVolumeFractionEvaluator(self.input_parser, initial_state['model'], initial_state['decoder'])
        initial_state['loss'] = loss

        #Optimizer
        if user_input_data['optimizer'].lower() == 'adam':
            if 'gradient_filtering' in user_input_data and user_input_data['gradient_filtering'].lower() == 'on':
                optimizer_object = optimizer.AdamOptimizerGradientFiltering(self.input_parser, initial_state['loss'])
            else:
                optimizer_object = optimizer.AdamOptimizer(self.input_parser, initial_state['loss'])
        elif user_input_data['optimizer'].lower() == 'lbfgs':
                optimizer_object = optimizer.LBFGSOptimizer(self.input_parser, initial_state['loss'])
        initial_state['optimizer'] = optimizer_object    
        
        #Timestepper
        if user_input_data['optimizer'].lower() == 'adam':
            timestepper = mr.ADAMTimeStepper(self.input_parser, initial_state['model'], initial_state['optimizer'], initial_state['decoder'])
        elif user_input_data['optimizer'].lower() == 'lbfgs':
            timestepper = mr.LBFGSTimeStepper(self.input_parser, initial_state['model'], initial_state['optimizer'], initial_state['decoder'])
        initial_state['timestepper'] = timestepper    

        return initial_state

if __name__ == "__main__":
    program = Main(sys.argv[1])
    program.run()


