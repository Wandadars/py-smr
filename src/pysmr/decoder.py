import logging
import PIL

import numpy as np
import sklearn.cluster as sklcluster

import image_processing_utils as ipu

logger = logging.getLogger(__name__)


class MicrostructureDecoder:
    """
    This class has the responsiblity of taking an image that is generated
    from the style transfer and converting the image back into a discrete
    set of pixel labels.
    """
    def __init__(self, input_file_parser):
        self.user_input_data = input_file_parser.user_input_data

    def decode_image(self):
        raise NotImplementedError

    def write_decoded_image(self):
        raise NotImplementedError

    def write_decoded_image(self, image):
        out = image.astype('uint8') # Normalize for display
        fig = plt.figure()
        fig.imshow(out)
        fig.savefig('decoded_image' + '.png')
        plt.close(fig)


class ClipDecoder(MicrostructureDecoder):
    """
    Implements the decoding processing by using a simple clip that
    constrains the array values in the image to be between [0,255] and
    casts the result to the unsigned 8 bit integer data type(which has
    a range of 0 to 255).
    """
    def __init__(self, input_file_parser):
        super().__init__(input_file_parser)
        self.num_phases = 2
        self.phase_colors = None

    def decode_image(self, image):
        return np.clip(image, 0, 255).astype('uint8')


class TwoPhaseMicrostructureDecoderNoDespeckling(MicrostructureDecoder):
    def __init__(self, input_file_parser):
        super().__init__(input_file_parser)
        self.num_phases = 2
        self.phase_colors = None

    def decode_image(self, image):
        """
        Image is a numpy array
        """
        remapped_image = ipu.map_pixels_to_discrete_values(image, self.phase_colors)
        return remapped_image.astype('uint8')

    def set_phase_colors(self, phase_colors):
        self.phase_colors = phase_colors


class TwoPhaseMicrostructureDecoder(MicrostructureDecoder):
    def __init__(self, input_file_parser):
        super().__init__(input_file_parser)
        self.num_phases = 2
        self.phase_colors = None
        self.lee_filter_size = 5

    def decode_image(self, image):
        """
        Image is a numpy array
        """
        despeckled_image = ipu.lee_filter(image, self.lee_filter_size)
        remapped_image = self.binarize_image(despeckled_image)
        return remapped_image.astype('uint8')
    
    def binarize_image(self, image):
        """
        Returns an image that is thresholded to discrete colors from an input that is continuous
        """
        return ipu.map_pixels_to_discrete_values(image, self.phase_colors)

    def set_phase_colors(self, phase_colors):
        self.phase_colors = phase_colors
    

class ThreePhaseMicrostructureDecoder(MicrostructureDecoder):
    def __init__(self, input_file_parser):
        super().__init__(input_file_parser)
        self.num_phases = 3
        self.phase_colors = None

    def decode_image(self, image):
        """
        Image is a numpy array
        """
        despeckled_image = ipu.lee_filter(image, 11)
        remapped_image = ipu.map_pixels_to_discrete_values(despeckled_image, self.phase_colors)
        return remapped_image.astype('uint8')

    def set_phase_colors(self, phase_colors):
        self.phase_colors = phase_colors


