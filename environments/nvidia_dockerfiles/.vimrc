"Scroll Wheel = Up/Down 1 lines"
filetype plugin indent on
" show existing tab with 4 spaces width"
set tabstop=4
" when indenting with '>', use 4 spaces width"
set shiftwidth=4
"Sets the number of columns for a TAB"
set softtabstop=4   
" On pressing tab, insert 4 spaces"
set expandtab
