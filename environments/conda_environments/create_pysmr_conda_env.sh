conda create --name pysmr_test

activate pysmr_test

conda install tensorflow-gpu pillow pytest scikit-image keras jupyter
pip install tox
pip install logger

deactivate pysmr_test
